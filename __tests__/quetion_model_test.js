import Question from "../modules/question/question_model.js";

test('Test question data getAll', async () => {
	const expectResult = [{"id":1,"title":"Question 1 ","content":"What is your name?","is_answered":"0","add_datetime":"2022-03-11T21:17:56.000Z"},{"id":2,"title":"Q2","content":"Where are you from?","is_answered":"0","add_datetime":"2022-03-11T21:17:56.000Z"},{"id":3,"title":"Ask Why","content":"Why do you do this?","is_answered":"0","add_datetime":"2022-03-11T21:19:40.000Z"}];
  	let questionModel = new Question();
  	const data = await questionModel.getAll();
  	expect(data).toStrictEqual(expectResult);
});


