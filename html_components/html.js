
const html_array = {"header":"", "title":"", "body": ""};

exports.header = (header) => {
	html_array["header"] = header;
};

exports.title = (title) => {
	html_array["title"] = title;
};

exports.body = (body) => {
	html_array["body"] = body;
}

exports.html = () => {
	
	head_str = "<head><title>" + html_array["title"] + "</title></head>";
	html_str = "<!DOCTYPE html><html>" + head_str;
	html_str = html_str + "<body>" + html_array["body"] + "</body></html>";
	return html_str;
}