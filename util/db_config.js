
const dbConfig = {
  HOST: "localhost",
  USER: "root",
  PASSWORD: "rootpass",
  DB: "testdb"
};

export default dbConfig;