import MysqlDb from "../../util/db.js";

// constructor
class Question {

  	constructor() {
		this.mysqldb = new MysqlDb();
		this.db = this.mysqldb.getConnection();
    }  
    
  	async getAll (title) {
  		let query = "SELECT * FROM question";
  		if (title) {
    		query += ` WHERE title LIKE '%${title}%'`;
  		}

		return this.doQuery(query)
	}
	
	async create (table, data) {
		let query = "INSERT INTO " + table + " ";
		let columns = "(";
		let valueStr = " VALUES(";
		let values = [];
		let count = 0;
		for (var key in data) {
			if (data.hasOwnProperty(key)) {
				if (count == 0) {
					columns = columns + key;
					valueStr = valueStr + "?";
				} else {
					columns = columns + " ," + key;
					valueStr = valueStr + " ," + "?";
				}
				values.push(data[key]);
   			}
   			count++;
		}
		columns = columns + ")";
		valueStr = valueStr + ")";
		
		query = query + columns + valueStr;
		
		console.log(query);
		console.log(values);
		
		return this.doQueryParams(query, values);
	}
	
	async update (table, andWheres ,data) {
		let query = "UPDATE " + table + " SET ";
		let columnStr = "";
		let whereStr = " WHERE ";
		let values = [];
		let count = 0;
		for (var key in data) {
			if (data.hasOwnProperty(key)) {
				if (count == 0) {
					columnStr = columnStr + key + " = ?";
				} else {
					columnStr = columnStr + " ," + key + " = ?";
				}
				values.push(data[key]);
   			}
   			count++;
		}
		
		count = 0;
		for (var key1 in andWheres) {
			if (andWheres.hasOwnProperty(key1)) {
				if (count == 0) {
					whereStr = whereStr + key1 + " = ?";
				} else {
					whereStr = whereStr + " AND " + key1 + " = ?";
				}
				values.push(andWheres[key1]);
   			}
   			count++;
		}

		query = query + columnStr + whereStr;
		
		console.log(query);
		console.log(values);
		
		return this.doQueryParams(query, values);
	}
		
	async doQuery(queryToDo) {
	    return new Promise( async (resolve,reject) => {
	      try {
	        const query = queryToDo; 
	        //this.db.connect();
	        this.db.query(query, (err, result) => {
	        //sqlDb.query(query, (err, result) => {
	          if (err) {
				  reject(err);
			  } else {
				//this.db.end();
				resolve(Object.values(JSON.parse(JSON.stringify(result))));
			  }
	        });
	      } catch (err) {
	        console.log(err);
	        reject(err)
	      }
	    })
	}
	
	async doQueryParams(sqlQuery, params) {
      return new Promise(async (resolve,reject) => {
	
		 try {
	        const query = sqlQuery; 
	        //this.db.connect();
	        this.db.query(query, params, (err, result) => {
	          if (err) {
				  reject(err);
			  } else {
				//this.db.end();
				resolve(Object.values(JSON.parse(JSON.stringify(result))));
			  }
	        });
	      } catch (err) {
	        console.log(err);
	        reject(err)
	      }

      })
      //return pro.then((val) => {
      //  return val;
      //})
    }
		
}



export default Question;

