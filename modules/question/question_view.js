class View {

  	constructor() {
	  this.title = "";
	  this.body = "";
    }  

	
	setTitle (title) {
		this.title = title;
	}
	
	setBody(body) {
	   this.body = body;
	}
	
	setDiv(data) {
		let divHtml = "";
		for (var key in data) {
   			if (data.hasOwnProperty(key)) {
				divHtml = divHtml + "<div id='q_" +  data[key].id + "' class='q_title' >" + data[key].title + "</div>";
   			}
		}
		divHtml = "<div>" + divHtml + "</div>";
		this.setBody(divHtml);
	}
	
	render() {
		let htmlText = "<html><head><title>" + this.title + "</title></head><body>";
		htmlText = htmlText + this.body + "</body></html>";
		return htmlText; 
	}
}

export default View;