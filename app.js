//Author: Hung-Fu Chan
//
//This follows ES6 syntax therefore we cannot use "require" to import module
//
import express from 'express';
import QuestionController from "./modules/question/question_controller.js";

//__dirname cannot be used when we use import in ES
//__dirname can be used when you just use require('path')
//So, we need the following fixes. These fixes are directly calling the path method.
import * as path from 'path';
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();

//Set the static css folder for public access
app.use('/css', express.static(path.join(__dirname, 'static/css')))

//Serve the static html file
app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'static/html/index.html')))

app.get('/user_question', (req, res) => {

	let questionCtrl = new QuestionController();
  	questionCtrl.findAll(req, res);

})

app.get('/user_question2', (req, res) => {

	let questionCtrl = new QuestionController();
  	questionCtrl.findAllWithStr(req, res);

})

app.get('/submit_question', (req, res) => {
  let questionCtrl = new QuestionController();
  questionCtrl.submitQuestion(req, res);
})

app.get('/update_question', (req, res) => {
	console.log("inside update");
   let questionCtrl = new QuestionController();
   questionCtrl.updateQuestionById(req, res);
})


app.get('/about', (req, res) => {
  res.send('about')
})


export default app

